/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.core;

import com.db.LoggerExample;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{

    private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());

    public static void log(String msg)
    {
        LOGGER.info(msg);
        System.out.println(msg);
    }

}
