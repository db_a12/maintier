/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.core;

import com.db.CounterDeals;
import com.db.CounterParty;
import com.db.Deal;
import com.db.Instrument;
import com.db.InstrumentCount;
import com.db.TimeBuySell;
import com.db.TimeValue;
import com.db.core.DBConnector;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author Graduate
 */
public class TableDAO {

    public TableDAO() {
        //
    }

    public ArrayList<CounterParty> getAllCounterParty() {
        System.out.println("called");
        try {
            Connection con = DBConnector.getConnector().getConnection();
            ArrayList<CounterParty> counterparties = new ArrayList<CounterParty>();
            String query = "select * from counterparty";
            PreparedStatement st = con.prepareStatement(query);
            ResultSet result = st.executeQuery();
            if (result == null) {
                System.out.print("result was null");
            }

            while (result != null && result.next()) {
                counterparties.add(new CounterParty(result.getInt(1), result.getString(2), result.getString(3)));
            }
            return counterparties;
        } catch (Exception e) {
            System.out.println("problem occured");
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Deal> getAllDeals() {
        try {
            ArrayList<Deal> deals = new ArrayList<Deal>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "select * from deal ";
            PreparedStatement st = con.prepareStatement(query);
            ResultSet results = st.executeQuery();
            if (results == null) {
                System.out.println("result was null");
            }
            while (results != null && results.next()) {
                deals.add(new Deal(results.getInt("deal_id"), results.getString("deal_type"), results.getDouble("deal_amount"), results.getInt("deal_quantity"), results.getInt("deal_counterparty_id"), results.getInt("deal_instrument_id")));
            }
            return deals;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return null;
    }

    public ArrayList<Instrument> getAllInstrument() {
        try {
            ArrayList<Instrument> deals = new ArrayList<Instrument>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "select * from instrument";
            PreparedStatement st = con.prepareStatement(query);
            ResultSet results = st.executeQuery();
            if (results == null) {
                System.out.println("result was null");
            }
            while (results != null && results.next()) {
                deals.add(new Instrument(results.getInt(1), results.getString(2)));
            }
            return deals;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        return null;
    }

    public String randomPrint() {
        return "trying random function";
    }

    public ArrayList<TimeValue> getBuy(int instrumentID) {
        try {
            ArrayList<TimeValue> buyToDate = new ArrayList<TimeValue>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT AVG(d.deal_amount) price, d.deal_time FROM deal d WHERE d.deal_instrument_id = ? AND d.deal_type = ? GROUP BY d.deal_time ORDER BY d.deal_time ASC;";
            PreparedStatement st = con.prepareStatement(query);
            st.setInt(1, instrumentID);
            st.setString(2, "B");
            ResultSet results = st.executeQuery();

            SimpleDateFormat dtFormater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            while (results.next()) {
                // String d = results.getString(2).replaceAll("T", " ");
                //   System.out.println(d);
                // Date theDate = dtFormater.parse(d);
                //  long ts = theDate.getTime();
                //      System.out.println(ts);
                buyToDate.add(new TimeValue(results.getString(2), results.getDouble(1)));

            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<CounterDeals> getCounterDeals(String counterName) {
        try {
            ArrayList<CounterDeals> buyToDate = new ArrayList<CounterDeals>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "select d.deal_type, i.instrument_name, d.deal_amount, d.deal_quantity FROM deal d\n"
                    + "INNER JOIN instrument i ON (d.deal_instrument_id = i.instrument_id)\n"
                    + "INNER JOIN counterparty c ON (d.deal_counterparty_id = c.counterparty_id)\n"
                    + "WHERE c.counterparty_name = ?\n"
                    + "ORDER BY d.deal_time DESC LIMIT 100;";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, counterName);
            ResultSet results = st.executeQuery();
            while (results.next()) {
                buyToDate.add(new CounterDeals(results.getString("deal_type"),results.getString("instrument_name"),results.getDouble("deal_amount"),results.getInt("deal_quantity")));
                             
            }   
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<TimeValue> getSell(int instrumentID) {
        try {
            ArrayList<TimeValue> buyToDate = new ArrayList<TimeValue>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT AVG(d.deal_amount) price, d.deal_time FROM deal d WHERE d.deal_instrument_id = ? AND d.deal_type = ? GROUP BY d.deal_time ORDER BY d.deal_time ASC;";
            PreparedStatement st = con.prepareStatement(query);
            st.setInt(1, instrumentID);
            st.setString(2, "S");
            ResultSet results = st.executeQuery();
            SimpleDateFormat dtFormater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            while (results.next()) {
                buyToDate.add(new TimeValue(results.getString(2), results.getDouble(1)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<TimeBuySell> getBuySell(int instrumentID) {
        try {
            ArrayList<TimeBuySell> buyToDate = new ArrayList<TimeBuySell>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT AVG(d.deal_amount) price, d.deal_time, d.deal_type FROM deal d WHERE d.deal_instrument_id = ? GROUP BY d.deal_time, d.deal_type ORDER BY d.deal_time ASC;";
            PreparedStatement st = con.prepareStatement(query);
            st.setInt(1, instrumentID);
            ResultSet results = st.executeQuery();
            SimpleDateFormat dtFormater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            while (results.next()) {
                buyToDate.add(new TimeBuySell(results.getString(2), results.getString(3), results.getDouble(1)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<InstrumentCount> countBuySell(String bs, String counterPartyName) {
        try {
            ArrayList<InstrumentCount> buyToDate = new ArrayList<InstrumentCount>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT i.instrument_name, COUNT(d.deal_amount) deal_count FROM deal d JOIN counterparty c ON (d.deal_counterparty_id = c.counterparty_id) JOIN instrument i ON (d.deal_instrument_id = i.instrument_id) WHERE deal_type = ? AND c.counterparty_name = ? GROUP BY c.counterparty_name, d.deal_type, i.instrument_name ORDER BY deal_count DESC;";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, bs);
            st.setString(2, counterPartyName);
            ResultSet results = st.executeQuery();

            while (results.next()) {
                buyToDate.add(new InstrumentCount(results.getString(1), results.getInt(2)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<InstrumentCount> getProfForCountRealize(String name) {
        try {
            ArrayList<InstrumentCount> buyToDate = new ArrayList<InstrumentCount>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT i.instrument_name, SUM(\n"
                    + "\n"
                    + "CASE\n"
                    + "\n"
                    + "  WHEN d.deal_type = 'S' THEN d.deal_amount\n"
                    + "  WHEN d.deal_type = 'B' THEN -d.deal_amount\n"
                    + "END\n"
                    + "\n"
                    + ") realised_profit\n"
                    + "FROM deal d\n"
                    + "\n"
                    + "JOIN counterparty c ON (d.deal_counterparty_id = c.counterparty_id)\n"
                    + "\n"
                    + "JOIN instrument i ON (d.deal_instrument_id = i.instrument_id)\n"
                    + "\n"
                    + "WHERE c.counterparty_name = ?\n"
                    + "GROUP BY c.counterparty_name, i.instrument_name;";
            PreparedStatement st = con.prepareStatement(query);
            st.setString(1, name);

            ResultSet results = st.executeQuery();

            while (results.next()) {
                buyToDate.add(new InstrumentCount(results.getString(1), results.getInt(2)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<InstrumentCount> getCounterProfits() {
        try {
            ArrayList<InstrumentCount> buyToDate = new ArrayList<InstrumentCount>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT i.instrument_name, SUM(\n"
                    + "\n"
                    + "CASE\n"
                    + "\n"
                    + "  WHEN d.deal_type = 'S' THEN d.deal_amount\n"
                    + "  WHEN d.deal_type = 'B' THEN -d.deal_amount\n"
                    + "END\n"
                    + "\n"
                    + ") realised_profit\n"
                    + "FROM deal d\n"
                    + "\n"
                    + "JOIN counterparty c ON (d.deal_counterparty_id = c.counterparty_id)\n"
                    + "\n"
                    + "JOIN instrument i ON (d.deal_instrument_id = i.instrument_id)\n"
                    + "\n"
                    + "GROUP BY  i.instrument_name;";
            PreparedStatement st = con.prepareStatement(query);

            ResultSet results = st.executeQuery();

            while (results.next()) {
                buyToDate.add(new InstrumentCount(results.getString(1), results.getInt(2)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<InstrumentCount> getStakeholderProfits() {
        try {
            ArrayList<InstrumentCount> buyToDate = new ArrayList<InstrumentCount>();
            Connection con = DBConnector.getConnector().getConnection();
            String query = "SELECT c.counterparty_name, SUM(\n"
                    + "\n"
                    + "CASE\n"
                    + "\n"
                    + "  WHEN d.deal_type = 'S' THEN d.deal_amount\n"
                    + "  WHEN d.deal_type = 'B' THEN -d.deal_amount\n"
                    + "END\n"
                    + "\n"
                    + ") realised_profit\n"
                    + "FROM deal d\n"
                    + "\n"
                    + "JOIN counterparty c ON (d.deal_counterparty_id = c.counterparty_id)\n"
                    + "\n"
                    + "JOIN instrument i ON (d.deal_instrument_id = i.instrument_id)\n"
                    + "\n"
                    + "GROUP BY c.counterparty_name;";
            PreparedStatement st = con.prepareStatement(query);

            ResultSet results = st.executeQuery();

            while (results.next()) {
                buyToDate.add(new InstrumentCount(results.getString(1), results.getInt(2)));
            }
            return buyToDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
