package com.db.core;

import com.db.core.DBConnector;
import java.sql.*;
import java.util.ArrayList;

public class LoginDAO {

	private static String userTableName = "users";
            
        /**
         * Test
         * @param username
         * @param password
         * @return
         * @throws SQLException 
         */
	public boolean validUser(String username, String password) throws SQLException
	{
		
		try
		{
			Connection con = DBConnector.getConnector().getConnection();
			if(con == null)
			{
				return false;
			}
			String query = "select * from users where user_id = ? and user_pwd = ?";
			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, username);
			st.setString(2, password);
			//Statement st = con.createStatement();
			ResultSet result = st.executeQuery();
			if(result == null)
			{
				return false;
			}
			
			if(result.next() != false)
			{
			//System.out.println(result.ge	tString(1));
			String pas = result.getString("user_pwd");
			if(pas != null)
			{
				return true;
			}		
			}
			else
			{
				return false;
			}
		}
		catch(Exception se) {
			System.out.println(se.getMessage());
			return false;
		}
		return false;
	}
}
