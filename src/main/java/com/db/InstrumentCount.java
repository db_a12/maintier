/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

/**
 *
 * @author Graduate
 */
public class InstrumentCount {

    private String instrument_name;
    private int instrument_count;

    public InstrumentCount(String instrument_name, int instrument_count) {
        this.instrument_name = instrument_name;
        this.instrument_count = instrument_count;
    }
    
    public String getInstrument_name() {
        return instrument_name;
    }

    public void setInstrument_name(String instrument_name) {
        this.instrument_name = instrument_name;
    }

    public int getInstrument_count() {
        return instrument_count;
    }

    public void setInstrument_count(int instrument_count) {
        this.instrument_count = instrument_count;
    }
   
}
