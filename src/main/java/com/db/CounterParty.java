/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

import java.sql.Date;

/**
 *
 * @author Graduate
 */
public class CounterParty {
    
     int counterparty_id;
     String counterparty_name;
     String counterparty_status;
     //Date counterparty_date_registered;

    public CounterParty(int counterparty_id, String counterparty_name, String counterparty_status) {
        this.counterparty_id = counterparty_id;
        this.counterparty_name = counterparty_name;
        this.counterparty_status = counterparty_status;
       // this.counterparty_date_registered = counterparty_date_registered;
    }
    
       public int getCounterparty_id() {
        return counterparty_id;
    }

    public void setCounterparty_id(int counterparty_id) {
        this.counterparty_id = counterparty_id;
    }
    public String getCounterparty_name() {
        return counterparty_name;
    }

    public void setCounterparty_name(String counterparty_name) {
        this.counterparty_name = counterparty_name;
    }

    public String getCounterparty_status() {
        return counterparty_status;
    }

    public void setCounterparty_status(String counterparty_status) {
        this.counterparty_status = counterparty_status;
    }

   
    
    @Override
    public String toString()
    {
        return "" + counterparty_id + ", " + counterparty_name + ", " + counterparty_status;
    }
}
