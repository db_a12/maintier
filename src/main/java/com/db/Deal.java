/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

/**
 *
 * @author Graduate
 */
public class Deal {
    private int deal_id;
    private int deal_counterparty_id;
    private int deal_instrument_id;
    private String deal_type;
    private double deal_amount;
    private int deal_quantity;

    

    public Deal(int deal_id, String deal_type, double deal_amount, int deal_quantity, int deal_counterparty_id, int deal_instrument_id) {
        this.deal_id = deal_id;
        this.deal_counterparty_id = deal_counterparty_id;
        this.deal_instrument_id = deal_instrument_id;
        this.deal_type = deal_type;
        this.deal_amount = deal_amount;
        this.deal_quantity = deal_quantity;
    }
    
     public int getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(int deal_id) {
        this.deal_id = deal_id;
    }

    public int getDeal_counterparty_id() {
        return deal_counterparty_id;
    }

    public void setDeal_counterparty_id(int deal_counterparty_id) {
        this.deal_counterparty_id = deal_counterparty_id;
    }

    public int getDeal_instrument_id() {
        return deal_instrument_id;
    }

    public void setDeal_instrument_id(int deal_instrument_id) {
        this.deal_instrument_id = deal_instrument_id;
    }

    public String getDeal_type() {
        return deal_type;
    }

    public void setDeal_type(String deal_type) {
        this.deal_type = deal_type;
    }

    public double getDeal_amount() {
        return deal_amount;
    }

    public void setDeal_amount(double deal_amount) {
        this.deal_amount = deal_amount;
    }
    public int getDeal_quantity() {
        return deal_quantity;
    }

    public void setDeal_quantity(int deal_quantity) {
        this.deal_quantity = deal_quantity;
    }
}
