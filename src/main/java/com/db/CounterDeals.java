/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db;

/**
 *
 * @author Graduate
 */
public class CounterDeals {

    private String deal_type;
    private String instrument_name;
    private double deal_amount;
    private int deal_quantity;

    public CounterDeals(String deal_type, String instrument_name, double deal_amount, int deal_quantity) {
        this.deal_type = deal_type;
        this.instrument_name = instrument_name;
        this.deal_amount = deal_amount;
        this.deal_quantity = deal_quantity;
    }

    public String getDeal_type() {
        return deal_type;
    }

    public void setDeal_type(String deal_type) {
        this.deal_type = deal_type;
    }

    public String getinstrument_name() {
        return instrument_name;
    }

    public void setinstrument_name(String instrument_name) {
        this.instrument_name = instrument_name;
    }

    public double getDeal_amount() {
        return deal_amount;
    }

    public void setDeal_amount(double deal_amount) {
        this.deal_amount = deal_amount;
    }

    public int getDeal_quantity() {
        return deal_quantity;
    }

    public void setDeal_quantity(int deal_quantity) {
        this.deal_quantity = deal_quantity;
    }

    
}
