/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.core;

import com.db.CounterDeals;
import com.db.CounterParty;
import com.db.Deal;
import com.db.InstrumentCount;
import com.db.TimeBuySell;
import com.db.TimeValue;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Graduate
 */
public class TableDAOTest {
    
    public TableDAOTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void getAllCounterPartyTest()
    {
//        System.out.println("came in");
        TableDAO dao = new TableDAO();
        ArrayList<CounterParty> counter = dao.getAllCounterParty();
        assertNotNull(counter);
        assertTrue(counter.size() > 0);
//        System.out.println("Size is " + counter.size());
        
//        for(CounterParty part : counter)
//        {
//            System.out.println(part.toString());
//        }
     
    }
    @Test
    public void getAllDealTest()
    {
//        System.out.println("came in");
        TableDAO dao = new TableDAO();
        ArrayList<Deal> counter = dao.getAllDeals();
        assertNotNull(counter);
        assertTrue(counter.size() > 0);
//        for(Deal part : counter)
//        {
//            System.out.println(part.getDeal_instrument_id() + "in " + part.getDeal_amount());
//        }
    }
    @Test
    public void testGetBuy()
    {
        TableDAO dao = new TableDAO();
        ArrayList<TimeValue> tv = dao.getBuy(2);
        assertNotNull(tv);
        
        for(TimeValue t: tv)
        {
            System.out.println(t.getTime() + " " + t.getValue());
        }
    }
      @Test
    public void testGetSell()
    {
        TableDAO dao = new TableDAO();
        ArrayList<TimeValue> tv = dao.getSell(2);
        assertNotNull(tv);
        
//        for(TimeValue t: tv)
//        {
//            System.out.println(t.getTime() + " " + t.getValue());
//        }
    }
         @Test
    public void testBuySell()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> tv = dao.countBuySell("B", "Selvyn");
        assertNotNull(tv);
//        for(InstrumentCount t: tv)
//        {
//            System.out.println(t.getInstrument_name() + " " + t.getInstrument_count());
//        }
    }
     @Test
    public void testGetCounter()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> tv = dao.getProfForCountRealize("Selvyn");
        assertNotNull(tv);
//        for(InstrumentCount t: tv)
//        {
//            System.out.println(t.getInstrument_name() + " " + t.getInstrument_count());
//        }
    }
         @Test
    public void testGetCounterProfits()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> tv = dao.getCounterProfits();
        assertNotNull(tv);
//         for(InstrumentCount t: tv)
//        {
//            System.out.println(t.getInstrument_name() + " " + t.getInstrument_count());
//        }
    }
     @Test
    public void testStakeholderProfits()
    {
        TableDAO dao = new TableDAO();
        ArrayList<InstrumentCount> tv = dao.getStakeholderProfits();
        assertNotNull(tv);
//         for(InstrumentCount t: tv)
//        {
//            System.out.println(t.getInstrument_name() + " " + t.getInstrument_count());
//        }
    }
       @Test
    public void testBuySellTogether()
    {
        TableDAO dao = new TableDAO();
        ArrayList<TimeBuySell> tv = dao.getBuySell(1);
        assertNotNull(tv);
//        for(TimeBuySell t: tv)
//        {
//            System.out.println(t.getPrice() + " " + t.getType() + " " + t.getTime());
//        }
    }
    @Test
    public void testCounterDeals()
    {
        TableDAO dao = new TableDAO();
        ArrayList<CounterDeals> tv = dao.getCounterDeals("Selvyn");
        assertNotNull(tv);
        
        for(CounterDeals t: tv)
        {
            System.out.println(t.getDeal_amount() + " " + t.getDeal_type());
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
